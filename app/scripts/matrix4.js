(function(global) {
  'use strict';
  var matrix4 = {
    zero: function() {
      return new Float32Array(16);
    },

    multiply: function(mat4A, mat4B) {
      var mat4C = new Float32Array(16);
      for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 4; j++) {
          for (var k = 0; k < 4; k++) {
            mat4C[j * 4 + i] = mat4C[j * 4 + i] + mat4A[k * 4 + i] * mat4B[j * 4 + k];
          }
        }
      }
      return mat4C;
    }
  };

  global.matrix4 = matrix4;

}(this));
